function afficher_texte(id) {
    var span = document.getElementById(id);
    if(span.style.display == "inline") {
      span.style.display = "none";
    } 
    else {
      span.style.display = "inline";
    }
  }

let cp = document.getElementById("cp");
let ic = document.getElementById("ic");
let fep = document.getElementById("fep");

let myScrollFunc = function () {
    let y = window.scrollY;
    if (y >= 200) {
      fep.className = "cadre_fep show"
    } 
    else {
      fep.className = "cadre_fep hide"
    }
    if (y >= 700) {
        cp.className = "cadre_cp show"
    } else {
        cp.className = "cadre_cp hide"
    }
    if (y >= 1200) {
      ic.className = "cadre_ic show"
    } 
    else {
      ic.className = "cadre_ic hide"
    }
};

window.addEventListener("scroll", myScrollFunc);