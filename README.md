# PortFolio Mathis Dore

Pour ce projet nous devions faire un Portfolio nous presentant en HTML5 et CSS3<br>pour nous promouvoir. J'ai decidé de le faire sous forme de "CV" amélioré<br>avec des liens cliquables, boutons, animations et un formulaire.

## Prérequis

Il faut cloner le dossier et le coller dans le dossier que vous voulez<br>dans votre ordi, vous pourrez ensuite ouvrir le .html dans le dossier.

## Attentes

Le site doit avoir une bonne interface utilisateur et respecter les normes HTLM5, <br> CSS3, et W3C. Il doit notamment contenir animation, flexbox et keyframe.

## Difficultés

- Lien script js
- Gestion de l'espace du body
- inspiration 

